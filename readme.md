Impulse Response Functions and Fish
=======
-------
##Description##

This is a relatively small project, so the readme will be short. This project is an extension of a bachelor thesis, which uses impulse response functions to identify the effect of shocks in oil price to the number of fish in the sea.

##Languages##

The statistics package R is used, so all analysis will be in .R files. Packrat is used to manage the dependencies in R. The manuscript is written in LaTeX, so .tex files contain the report. A BibTeX bibliography will also be included.

##To-Do##

* Continue writing the manuscript, primarily find new source material to expand on- and support the outline.
* Get the new dataset from Scott, including zooplankton data.
* It feels like including both climate and economic variables splits the message, as only economic variables can be manipulated (and as a result are the only interesting ones for policies).
* The results are of course preliminary, taken from the old model, I need to rerun with the new dataset.